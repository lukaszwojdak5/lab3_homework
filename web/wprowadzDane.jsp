<%-- 
    Document   : wprowadzDane
    Created on : 2022-06-02, 19:20:59
    Author     : wojdak
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP - wprowadzanie danych dla Beana</title>
    </head>
    <body>
        Wprowadz, dla ilu losowych liczb chcesz obliczyc średnią harmoczniną:
        <form action="wynik.jsp" method="POST">
            <input type="text" name="liczba">
            <input type="submit" value="Licz">
        </form>
    </body>
</html>
