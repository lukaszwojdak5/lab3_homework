<%-- 
    Document   : wynik
    Created on : 2022-06-02, 20:03:05
    Author     : wojdak
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>wynik</title>
    </head>
    <body>
        <% 
            String n = request.getParameter("liczba"); 
        %>
        <h1>Średnia harmoniczna wylosowanych danych: </h1>
        <jsp:useBean id="Bean" class="Lab3.NewBean" scope="session"></jsp:useBean>
        <jsp:setProperty name="Bean" property="liczbaPomiarow" value="<%=n %>"></jsp:setProperty>
        
        <%=Bean.liczSrednia()%>
        
        <form action="wprowadzDane.jsp" method="POST">
            <input type ="submit" value="Powrot">
        </form>
    </body>
</html>
