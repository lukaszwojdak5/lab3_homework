/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab3;

import java.util.Random;
import java.beans.*;
import java.io.Serializable;

/**
 *
 * @author wojdak
 */
public class NewBean implements Serializable {
    
    public static final String PROP_SAMPLE_PROPERTY = "sampleProperty";
    
    private String liczbaPomiarow;
    
    private PropertyChangeSupport propertySupport;
    
    public NewBean() {
        propertySupport = new PropertyChangeSupport(this);
    }
    
    public String getLiczbaPomiarow() {
        return liczbaPomiarow;
    }
    
    public void setLiczbaPomiarow(String value) {
        String oldValue = liczbaPomiarow;
        liczbaPomiarow = value;
        propertySupport.firePropertyChange(PROP_SAMPLE_PROPERTY, oldValue, liczbaPomiarow);
    }
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }
    
    public String liczSrednia(){
        float licznik = Float.parseFloat(liczbaPomiarow);
        float[] dane = generujDane();
        float mianownik = liczMianownik(dane);
        float srednia = licznik/mianownik;

        String wynikk = String.valueOf(srednia);
        return wynikk;
    }
    
    public float[] generujDane(){
        Random generator = new Random();
        float[] tablicaDanych = new float[Integer.parseInt(liczbaPomiarow)];
        
        for(int i = 0; i < Integer.parseInt(liczbaPomiarow); i++){
            tablicaDanych[i] = generator.nextInt(99);
        }
        return tablicaDanych;
    }
    
    public float liczMianownik(float[] t){
        float[] tab = t;
        float[] newTab = new float[Integer.parseInt(liczbaPomiarow)];
        for(int i = 0; i < Integer.parseInt(liczbaPomiarow); i++){
            newTab[i] = 1/tab[i];
        }
        float mianownik = 0;
        for(int i = 0; i < Integer.parseInt(liczbaPomiarow); i++){
            mianownik += newTab[i];
        }
        return mianownik;
    }
    
}
